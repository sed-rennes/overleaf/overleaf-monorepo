# overleaf-monorepo

## how to generate a new dev/inria branch starting from main/head of Overleaf depot

* Check what is the current version of Overleaf here : https://github.com/overleaf/overleaf/wiki#release-notes or here https://hub.docker.com/r/sharelatex/sharelatex/tags, we suppose here the current version is something like `a.b.c` (where a,b,c, are integers)
* il your machine console do :
```bash
# version to set
OVERLEAF_VERSION = a.b.c
# get the docker image (maybe take lot of time) 
docker pull sharelatex/sharelatex:$OVERLEAF_VERSION
# get associated git tag commit of this image
OVERLEAF_COMMIT=`docker image history --no-trunc sharelatex/sharelatex:$OVERLEAF_VERSION | grep "MONOREPO_REVISION=" | sed 's/.*MONOREPO_REVISION=\([a-zA-Z0-9]*\).*/\1/g'`
# remove this docker image 
docker rmi sharelatex/sharelatex:$OVERLEAF_VERSION

git clone git@gitlab.inria.fr:sed-rennes/overleaf/overleaf-monorepo.git
cd overleaf-monorepo/
git remote add source https://github.com/overleaf/overleaf.git
git fetch source main
git checkout $OVERLEAF_COMMIT
git checkout -b dev/inria-$OVERLEAF_VERSION
git push origin dev/inria-$OVERLEAF_VERSION
```

